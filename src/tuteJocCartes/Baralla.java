package tuteJocCartes;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Baralla {
	private List<Carta> cartes;

    public Baralla() {
        cartes = new ArrayList<>();

        for (Pal pal : Pal.values()) {
            for (Valor valor : Valor.values()) {
                cartes.add(new Carta(valor, pal));
            }
        }
    }
    
    public void barallar() {
        Collections.shuffle(cartes);
    }
    
    public Carta agafarCarta() {
        if (cartes.isEmpty()) {
            return null;
        }

        return cartes.remove(cartes.size() - 1);
    }

    public int getNombreCartes() {
        return cartes.size();
    }
}


