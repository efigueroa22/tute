package tuteJocCartes;

public class Carta {
	private Valor valor;
    private Pal pal;

    public Carta(Valor valor, Pal pal) {
        this.valor = valor;
        this.pal = pal;
    }

    public Valor getValor() {
        return valor;
    }

    public Pal getPal() {
        return pal;
    }

    @Override
    public String toString() {
        return valor + " de " + pal;
    }
}