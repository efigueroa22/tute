package tuteJocCartes;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Tute {
	private Baralla baralla;
    private List<Carta> maJugador;
    private int intentsCanvi;
    
    int cavalls = 0;
    int reis = 0;
    
    public Tute() {
        baralla = new Baralla();
        maJugador = new ArrayList<>();
        intentsCanvi = 20;
    }
    
    public void jugar() {
        baralla.barallar();

        while (maJugador.size() < 4) {
            Carta carta = baralla.agafarCarta();
            
            if (carta != null) {
                maJugador.add(carta);
                System.out.println("Has agafat la carta: " + carta);
            } else {
                System.out.println("La baralla està buida.");
                break;
            }
        }
        
        sumarCavallsReis();

        if (cavalls == 4) {
            System.out.println("Has guanyat amb els 4 cavalls!");
        } 
        else if (reis == 4) {
            System.out.println("Has guanyat amb els 4 reis!");
        }
        else {
            System.out.println("No has guanyat amb cap combinacio.");
            
            Scanner sc = new Scanner(System.in);
            
            for (int i = 0; i < intentsCanvi; i++) {
                System.out.println("Intents de canvi restants: " + (intentsCanvi - i));
                System.out.println("Vols canviar alguna carta? (S/N)");
                String resposta = sc.nextLine();
                
                if (resposta.equalsIgnoreCase("S")) {
                    System.out.println("Quina carta vols canviar? (1-4)");
                    int numeroCarta = sc.nextInt();
                    sc.nextLine();
                    
                    if (numeroCarta >= 1 && numeroCarta <= 4) {
                        Carta novaCarta = baralla.agafarCarta();
                        
                        if (novaCarta != null) {
                            Carta cartaAntiga = maJugador.get(numeroCarta - 1);
                            maJugador.set(numeroCarta - 1, novaCarta);
                            System.out.println("Has canviat la carta " + cartaAntiga + " per la carta " + novaCarta);
                            
                            sumarCavallsReis();
                            
                            if (cavalls == 4) {
                                System.out.println("Has guanyat amb els 4 cavalls!");
                                return;
                            } 
                            else if (reis == 4) {
                                System.out.println("Has guanyat amb els 4 reis!");
                                return;
                            }
                        } 
                        else {
                            System.out.println("No queden cartes a la baralla per a fer el canvi.");
                            break;
                        }
                    } 
                    else {
                        System.out.println("Opció invàlida. Tria un número entre 1 i 4.");
                        i--;
                    }
                } 
                else if (resposta.equalsIgnoreCase("N")) {
                    break;
                } 
                else {
                    System.out.println("Opció invàlida. Tria 'S' per a canviar cartes o 'N' per a continuar sense canvis.");
                    i--;
                }
                
                if(i == intentsCanvi - 1) {
                	System.out.println("No et queden mes intents");
                }
            }
            sc.close();
            
        }
    }
    
    public void sumarCavallsReis() {
    	cavalls = 0;
    	reis = 0;
        for (Carta carta : maJugador) {
            if (carta.getValor() == Valor.CAVALL) {
                cavalls++;
            } else if (carta.getValor() == Valor.REI) {
                reis++;
            }
        }
    }
}
