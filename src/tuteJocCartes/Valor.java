package tuteJocCartes;

public enum Valor {
	AS,
    DOS,
    TRES,
    QUATRE,
    CINC,
    SIS,
    SET,
    SOTA,
    CAVALL,
    REI
}
